# Docker containers for Drupal local Development
Dontainers simple test, including:

- php 7.0
- nginx
- mariaDB

The repository also contains:

- Drupal 8
- Makefile to simplify its use

## Requeriments

- [git](https://git-scm.com/downloads)
- [Docker for mac](https://www.docker.com/docker-mac) | [Docker for windows](https://www.docker.com/docker-windows)
- [Composer installed globally](https://getcomposer.org/doc/00-intro.md)
- *Optional* If you want to run Drush commands: [Drush installed globally](http://docs.drush.org/en/8.x/install-alternative/)

## Instructions

1. Clone the repository:

    ``` bash
    git clone git@gitlab.com:mobomopub/drocker.git 
    ```
2. cd into repository directory and 'run' containers:

    ``` bash
    cd drocker && make up 
    ```
3. Check containers are running:

    ``` bash
    make status
    ```
4. Using your preferred browser go to: [http://localhost:8080](http://localhost:8080)
    You should get the drupal installation screen.

