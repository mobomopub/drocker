# Init global Vars
DOCKER_COMPOSE_FILE := docker-compose.yml
##
# Docker
##
# Docker up containers
up: 
	docker-compose -f ${DOCKER_COMPOSE_FILE} up -d --build

# Docker stop containers
stop: 
	docker-compose stop

# Docker rm containers
rm: 
	docker-compose rm

# Docker processes
ps: 
	docker-compose -f ${DOCKER_COMPOSE_FILE} ps

# Docker logs
logs: 
	docker-compose -f ${DOCKER_COMPOSE_FILE} logs

##
# Drush
##
# Drush Status
dst: 
	docker-compose -f ${DOCKER_COMPOSE_FILE} exec php drush --root=/var/www/html status


